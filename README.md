# Media-Timeline

Simple timeline for media-element based on the [Vis.js timeline](http://visjs.org/docs/timeline.html) and the [player adapter](https://github.com/entwinemedia/annotations/wiki/Player-adapter-API) from the [annotations-tool](https://github.com/entwinemedia/annotations).


## Demo
To launch the demo, you have to [install Grunt](http://gruntjs.com/getting-started).
In the repository folder, you can then install the npm dependencies and launch the default grunt task

```
#!bash
npm install
grunt
```

and then open your browser at http://localhost:9001/example/.