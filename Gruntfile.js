module.exports = function (grunt) {

    /** ================================================
     *  Project configuration
     ==================================================*/
    grunt.initConfig({
        /** Load informations from package.json */
        pkg: grunt.file.readJSON('package.json'),

        /** JSHint properties */
        jshintProperties: grunt.file.readJSON('.jshintrc'),


        /** The current target file for the watch tasks */
        currentWatchFile: '',

        /** Paths for the different types of ressource */
        srcPath: {
            js      : 'src/**/*.js',
            html    : '**/*.html',
            example : 'example/*'
        },

        /** Web server */
        connect: {
            default: {
                options: {
                    port: 9001,
                    base: '.',
                    keepalive: true,
                    livereload: true
                }
            }
        },

        /** Install Bower dependancies */
        bower: {
            install: {
            }
        },

        /** Lint task*/
        jshint: {
            all     : '<%= currentWatchFile %>',
            options : '<%= jshintProperties %>'
        },

        /** Uglify task */
        uglify: {
            default: {
              files: {
                'dest/media-timeline.min.js': ['src/mediaTimeline.js', 'src/playerAdapter/playerAdapter.js']
              }
            }
        },

        /** Task to watch src files and process them */
        watch: {
            options: {
                nospawn: true
            },

            // Watch Javascript files
            js: {
                files: ['<%= srcPath.js %>'],
                tasks: ['jshint:all', 'uglify']
            },

            // Watch file on web server for live reload
            www: {
                options: {
                    livereload: true,
                    nospawn: true
                },
                files: ['<%= srcPath.html %>', '<%= srcPath.js %>', '<%= srcPath.example %>']
            }
        },

        /** Task to run tasks in parrallel */
        concurrent: {
            default: {
                tasks: ['watch', 'connect'],
                options: {
                    logConcurrentOutput: true,
                    limit: 6
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-bower-task');

    // Default task
    grunt.registerTask('default', ['bower', 'uglify', 'concurrent']);
    grunt.registerTask('build', ['uglify']);


    /** ================================================
     *  Listerers 
     ==================================================*/

    // on watch events configure jshint:all to only run on changed file
    grunt.event.on('watch', function (action, filepath, target) {

        // Set the current file processed for the different tasks
        grunt.config.set('currentWatchFile', [filepath]);

        if (target == 'multiple') {
            // If the watch target is multiple, 
            // we manage the tasks to run following the touched file extension
            var ext = filepath.split('.').pop();

            switch (ext) {
                case 'js':
                    grunt.task.run('jshint');
                    break;
            }
        }
    });
};